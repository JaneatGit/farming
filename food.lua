local S = farming.intllib

--= Sugar

minetest.register_craftitem("farming:sugar", {
	description = S("Sugar"),
	inventory_image = "farming_sugar.png",
	groups = {food_sugar = 1, flammable = 3},
})

minetest.register_craft({
	type = "cooking",
	cooktime = 3,
	output = "farming:sugar 2",
	recipe = "default:papyrus",
})


--= Salt

minetest.register_node("farming:salt", {
	description = ("Salt"),
	inventory_image = "farming_salt.png",
	wield_image = "farming_salt.png",
	drawtype = "plantlike",
	visual_scale = 0.8,
	paramtype = "light",
	tiles = {"farming_salt.png"},
	groups = {food_salt = 1, vessel = 1, dig_immediate = 3,
			attached_node = 1},
	sounds = default.node_sound_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-0.25, -0.5, -0.25, 0.25, 0.3, 0.25}
	},
})

minetest.register_craft({
	type = "cooking",
	cooktime = 15,
	output = "farming:salt",
	recipe = "bucket:bucket_water",
	replacements = {{"bucket:bucket_water", "bucket:bucket_empty"}}
})

--= Rose Water

minetest.register_node("farming:rose_water", {
	description = ("Rose Water"),
	inventory_image = "farming_rose_water.png",
	wield_image = "farming_rose_water.png",
	drawtype = "plantlike",
	visual_scale = 0.8,
	paramtype = "light",
	tiles = {"farming_rose_water.png"},
	groups = {food_rose_water = 1, vessel = 1, dig_immediate = 3,
			attached_node = 1},
	sounds = default.node_sound_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-0.25, -0.5, -0.25, 0.25, 0.3, 0.25}
	},
})

minetest.register_craft({
	output = "farming:rose_water",
	recipe = {
		{"flowers:rose", "flowers:rose", "flowers:rose"},
		{"flowers:rose", "flowers:rose", "flowers:rose"},
		{"bucket:bucket_water", "group:food_pot", "vessels:glass_bottle"},
	},
	replacements = {
		{"bucket:bucket_water", "bucket:bucket_empty"},
		{"group:food_pot", "farming:pot"},
	}
})

--= Turkish Delight

minetest.register_craftitem("farming:turkish_delight", {
	description = S("Turkish Delight"),
	inventory_image = "farming_turkish_delight.png",
	groups = {flammable = 3},
	on_use = minetest.item_eat(2),
})

minetest.register_craft({
	output = "farming:turkish_delight 4",
	recipe = {
		{"group:food_gelatin", "group:food_sugar", "group:food_gelatin"},
		{"group:food_sugar", "group:food_rose_water", "group:food_sugar"},
		{"group:food_cornstarch", "group:food_sugar", "dye:pink"},
	},
	replacements = {
		{"group:food_cornstarch", "farming:bowl"},
		{"group:food_rose_water", "vessels:glass_bottle"},
	},
})

--= Garlic Bread

minetest.register_craftitem("farming:garlic_bread", {
	description = S("Garlic Bread"),
	inventory_image = "farming_garlic_bread.png",
	groups = {flammable = 3},
	on_use = minetest.item_eat(2),
})

minetest.register_craft({
	type = "shapeless",
	output = "farming:garlic_bread",
	recipe = {"group:food_toast", "group:food_garlic_clove", "group:food_garlic_clove"},
})

--= Donuts (thanks to Bockwurst for making the donut images)

minetest.register_craftitem("farming:donut", {
	description = S("Donut"),
	inventory_image = "farming_donut.png",
	on_use = minetest.item_eat(4),
})

minetest.register_craft({
	output = "farming:donut 3",
	recipe = {
		{"", "group:food_flour", ""},
		{"group:food_flour", "group:food_sugar", "group:food_flour"},
		{"", "group:food_flour", ""},
	}
})

minetest.register_craftitem("farming:donut_chocolate", {
	description = S("Chocolate Donut"),
	inventory_image = "farming_donut_chocolate.png",
	on_use = minetest.item_eat(6),
})

minetest.register_craft({
	output = "farming:donut_chocolate",
	recipe = {
		{'group:food_cocoa'},
		{'farming:donut'},
	}
})

minetest.register_craftitem("farming:donut_apple", {
	description = S("Apple Donut"),
	inventory_image = "farming_donut_apple.png",
	on_use = minetest.item_eat(6),
})

minetest.register_craft({
	output = "farming:donut_apple",
	recipe = {
		{'default:apple'},
		{'farming:donut'},
	}
})

--= Porridge Oats

minetest.register_craftitem("farming:porridge", {
	description = S("Porridge"),
	inventory_image = "farming_porridge.png",
	on_use = minetest.item_eat(6, "farming:bowl"),
})

minetest.after(0, function()

	local fluid = "bucket:bucket_water"
	local fluid_return = "bucket:bucket_water"

	if minetest.get_modpath("mobs") and mobs and mobs.mod == "redo" then
		fluid = "group:food_milk"
		fluid_return = "mobs:bucket_milk"
	end

	minetest.register_craft({
		type = "shapeless",
		output = "farming:porridge",
		recipe = {
			"farming:seed_barley", "farming:seed_barley", "farming:seed_wheat",
			"farming:seed_wheat", "group:food_bowl", fluid
		},
		replacements = {{fluid_return, "bucket:bucket_empty"}}
	})

	minetest.register_craft({
		type = "shapeless",
		output = "farming:porridge",
		recipe = {
			"group:food_oats", "group:food_oats", "group:food_oats",
			"group:food_oats", "group:food_bowl", fluid
		},
		replacements = {{fluid_return, "bucket:bucket_empty"}}
	})
end)

--= Jaffa Cake

minetest.register_craftitem("farming:jaffa_cake", {
	description = S("Jaffa Cake"),
	inventory_image = "farming_jaffa_cake.png",
	on_use = minetest.item_eat(6),
})

minetest.register_craft({
	type = "shapeless",
	output = "farming:jaffa_cake",
	recipe = {
		"farming:baking_tray", "group:food_egg", "group:food_sugar",
		"group:food_flour", "group:food_cocoa", "group:food_orange",
		"group:food_milk"
	},
	replacements = {
		{"farming:baking_tray", "farming:baking_tray"},
		{"mobs:bucket_milk", "bucket:bucket_empty"}
	}
})

-- Baguette Dough

minetest.register_craftitem("farming:dough_baguette", {
        description = S("Baguette Dough"),
        inventory_image = "farming_dough_baguette.png",
        groups = {food_dough = 1},
})

minetest.register_craft({
        output = "farming:dough_baguette",
        recipe = {
                {"farming:flour", "group:food_egg", ""},
                {"", "bucket:bucket_water", "farming:flour_multigrain"},
                {"", "", "farming:mixing_bowl"}
       	},
       	replacements = {
                {"bucket:bucket_water", "bucket:bucket_empty"},
		{"farming:mixing_bowl", "farming:mixing_bowl"},
	}
})

minetest.register_craft({
        output = "farming:dough_baguette",
        recipe = {
                {"farming:flour", "group:food_egg", ""},
                {"", "bucket:bucket_river_water", "farming:flour_multigrain"},
                {"", "", "farming:mixing_bowl"}
       	},
       	replacements = {
		{"bucket:bucket_river_water", "bucket:bucket_empty"},
		{"farming:mixing_bowl", "farming:mixing_bowl"},
	}
})

-- Baguette

minetest.register_craftitem("farming:baguette", {
	description = S("Baguette"),
	inventory_image = "farming_baguette.png",
	on_use = minetest.item_eat(14),
})

minetest.register_craft({
	type = "cooking",
	cooktime = 10,
	output = "farming:baguette",
	recipe = "farming:dough_baguette",
})

-- Croissant Dough

minetest.register_craftitem("farming:dough_croissant", {
	description = S("Croissant Dough"),
	inventory_image = "farming_dough_croissant.png",
	groups = {food_dough = 1},
})

minetest.register_craft({
	output = "farming:dough_croissant",
	recipe = {
		{"", "petz:honey_bottle", ""},
		{"farming:flour", "", "farming:flour"},
		{"group:food_milk", "", "farming:mixing_bowl"}
  	},
       	replacements = {
                {"group:food_milk", "bucket:bucket_empty"},
		{"petz:honey_bottle", "vessels:glass_bottle"},
		{"farming:mixing_bowl", "farming:mixing_bowl"},
	}
})

-- Croissant

minetest.register_craftitem("farming:croissant", {
	description = S("Croissant"),
	inventory_image = "farming_croissant.png",
	on_use = minetest.item_eat(12),
})

minetest.register_craft({
	type = "cooking",
	cooktime = 10,
	output = "farming:croissant",
	recipe = "farming:dough_croissant",
})
-- Whipping Cream

minetest.register_craftitem("farming:whipping_cream", {
	description = S("Whipping Cream"),
	inventory_image = "farming_whipping_cream.png",
	on_use = minetest.item_eat(4, "vessels:glass_bottle"),
})

minetest.register_craft({
	type = "shapeless",
	output = "farming:whipping_cream",
	recipe = {"petz:bucket_milk", "farming:mixing_bowl", "vessels:glass_bottle"},
       	replacements = {
                {"petz:bucket_milk", "bucket:bucket_empty"},
		{"farming:mixing_bowl", "farming:mixing_bowl"},
	},
})

-- Strawberry Shortcake 

minetest.register_craftitem("farming:strawberry_shortcake", {
	description = S("Strawberry Shortcake"),
	inventory_image = "farming_strawberry_shortcake.png",
	on_use = minetest.item_eat(16),
})

minetest.register_craft({
	output = "farming:strawberry_shortcake",
	recipe = {
		{"ethereal:strawberry", "farming:whipping_cream", "ethereal:strawberry"},
		{"farming:sugar", "farming:flour", "farming:sugar"},
		{"farming:flour", "petz:bucket_milk", "farming:flour"},
	},
       	replacements = {
                {"petz:bucket_milk", "bucket:bucket_empty"},
		{"farming:whipping_cream", "vessels:glass_bottle"},
	},
})
-- Tea

-- Mint Tea

minetest.register_node("farming:tea_mint", {
	description = S("Mint Tea"),
	inventory_image = "farming_tea_mint.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
	wield_image = "farming_tea_mint.png",
	drawtype = "plantlike",
	visual_scale = 0.8,
	paramtype = "light",
	walkable = false,
	tiles = {{
   		name = "farming_tea_mint_animated.png",
   		animation = {type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = 3.3}
}},
	groups = {farming_tea = 1, vessel = 1, dig_immediate = 3,
			attached_node = 1},

	sounds = default.node_sound_glass_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-0.25, -0.5, -0.25, 0.25, 0.3, 0.25}
	},
})

minetest.register_craft({ 
	type = "shapeless",
	output = "farming:tea_mint",
	recipe = {"vessels:drinking_glass", "africaplants:mint", "bucket:bucket_water", "farming:saucepan"},
	replacements = {
		{"bucket:bucket_water", "bucket:bucket_empty"},
		{"farming:saucepan", "farming:saucepan"},
	},

})

-- Nettle Tea

minetest.register_node("farming:tea_nettle", {
	description = S("Nettle Tea"),
	inventory_image = "farming_tea_nettle.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
	wield_image = "farming_tea_nettle.png",
	drawtype = "plantlike",
	visual_scale = 0.8,
	paramtype = "light",
	walkable = false,
	tiles = {{
   		name = "farming_tea_nettle_animated.png",
   		animation = {type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = 3.3}
}},
	groups = {farming_tea = 1, vessel = 1, dig_immediate = 3,
			attached_node = 1},

	sounds = default.node_sound_glass_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-0.25, -0.5, -0.25, 0.25, 0.3, 0.25}
	},
})

minetest.register_craft({ 
	type = "shapeless",
	output = "farming:tea_nettle",
	recipe = {"vessels:drinking_glass", "nettle:nettle", "bucket:bucket_water", "farming:saucepan"},
	replacements = {
		{"bucket:bucket_water", "bucket:bucket_empty"},
		{"farming:saucepan", "farming:saucepan"},
	},

})

-- Orange Tea

minetest.register_node("farming:tea_orange", {
	description = S("Orange Tea"),
	inventory_image = "farming_tea_orange.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
	wield_image = "farming_tea_orange.png",
	drawtype = "plantlike",
	visual_scale = 0.8,
	paramtype = "light",
	walkable = false,
	tiles = {{
   		name = "farming_tea_orange_animated.png",
   		animation = {type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = 3.3}
}},
	groups = {vessel = 1, dig_immediate = 3, attached_node = 1},

	sounds = default.node_sound_glass_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-0.25, -0.5, -0.25, 0.25, 0.25, 0.25}
	},
})

minetest.register_craft({
	output = "farming:tea_orange",
	recipe = {
		{"", "ethereal:orange", "africaplants:star_anise_plant"}, 
		{"", "bucket:bucket_water", "farming:saucepan"},
		{"", "", "vessels:drinking_glass"},
	},
	replacements = {
		{"bucket:bucket_water", "bucket:bucket_empty"},
		{"farming:saucepan", "farming:saucepan"},
	},
})

-- White Tea

minetest.register_node("farming:tea_white", {
	description = S("White Tea"),
	inventory_image = "farming_tea_white.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
	wield_image = "farming_tea_white.png",
	drawtype = "plantlike",
	visual_scale = 0.8,
	paramtype = "light",
	walkable = false,
	tiles = {{
   		name = "farming_tea_white_animated.png",
   		animation = {type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = 3.3}
}},
	groups = {farming_tea = 1, vessel = 1, dig_immediate = 3,
			attached_node = 1},

	sounds = default.node_sound_glass_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-0.25, -0.5, -0.25, 0.25, 0.3, 0.25}
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "farming:tea_white",
	recipe = {"vessels:drinking_glass", "farming:vanilla", "bucket:bucket_water", "farming:saucepan"},
	replacements = {
		{"bucket:bucket_water", "bucket:bucket_empty"},
		{"farming:saucepan", "farming:saucepan"},
	},
})

-- Hot Chocolate

minetest.register_node("farming:tea_hot_chocolate", {
	description = S("Hot Chocolate"),
	inventory_image = "farming_tea_hot_chocolate.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
	wield_image = "farming_tea_hot_chocolate.png",
	drawtype = "plantlike",
	visual_scale = 0.8,
	paramtype = "light",
	walkable = false,
	tiles = {{
   		name = "farming_tea_hot_chocolate_animated.png",
   		animation = {type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = 3.3}
}},
	groups = {farming_tea = 1, vessel = 1, dig_immediate = 3,
			attached_node = 1},

	sounds = default.node_sound_glass_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-0.25, -0.5, -0.25, 0.25, 0.3, 0.25}
	},
})

minetest.register_craft({
	output = "farming:tea_hot_chocolate",
	recipe = {
		{"", "farming:chocolate_dark", "farming:sugar"}, 
		{"", "petz:bucket_milk", "farming:saucepan"},
		{"", "", "vessels:drinking_glass"},
	},
	replacements = {
		{"petz:bucket_milk", "bucket:bucket_empty"},
		{"farming:saucepan", "farming:saucepan"},
	},
})

-- Strawberry Tea

minetest.register_node("farming:tea_strawberry", {
	description = S("Strawberry Tea"),
	inventory_image = "farming_tea_strawberry.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
	wield_image = "farming_tea_strawberry.png",
	drawtype = "plantlike",
	visual_scale = 0.8,
	paramtype = "light",
	walkable = false,
	tiles = {{
   		name = "farming_tea_strawberry_animated.png",
   		animation = {type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = 3.3}
}},
	groups = {farming_tea = 1, vessel = 1, dig_immediate = 3,
			attached_node = 1},

	sounds = default.node_sound_glass_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-0.25, -0.5, -0.25, 0.25, 0.3, 0.25}
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "farming:tea_strawberry",
	recipe = {"vessels:drinking_glass", "ethereal:strawberry", "bucket:bucket_water", "farming:saucepan"},
	replacements = {
		{"bucket:bucket_water", "bucket:bucket_empty"},
		{"farming:saucepan", "farming:saucepan"},
	},
})

-- Honey Lemon Tea

minetest.register_node("farming:tea_honey_lemon", {
	description = S("Honey Lemon Tea"),
	inventory_image = "farming_tea_honey_lemon.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
	wield_image = "farming_tea_honey_lemon.png",
	drawtype = "plantlike",
	visual_scale = 0.8,
	paramtype = "light",
	walkable = false,
	tiles = {{
   		name = "farming_tea_honey_lemon_animated.png",
   		animation = {type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = 3.3}
}},
	groups = {farming_tea = 1, vessel = 1, dig_immediate = 3,
			attached_node = 1},

	sounds = default.node_sound_glass_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-0.25, -0.5, -0.25, 0.25, 0.3, 0.25}
	},
})

minetest.register_craft({
	output = "farming:tea_honey_lemon",
	recipe = {
		{"", "lemontree:lemon", "petz:honey_bottle"}, 
		{"", "bucket:bucket_water", "farming:saucepan"},
		{"", "", "vessels:drinking_glass"},
	},
	replacements = {
		{"bucket:bucket_water", "bucket:bucket_empty"},
		{"farming:saucepan", "farming:saucepan"},
		{"petz:honey_bottle", "vessels:glass_bottle"}
	},	
})

-- Muffins 

-- Strawberry

minetest.register_craftitem("farming:muffin_strawberry", {
	description = S("Strawberry Muffin"),
	inventory_image = "farming_muffin_strawberry.png",
	on_use = minetest.item_eat(4),
})

minetest.register_craft({ 
	type = "shapeless",
	output = "farming:muffin_strawberry 2",
	recipe = {"ethereal:strawberry", "group:food_bread", "ethereal:strawberry"},
})

-- Banana

minetest.register_craftitem("farming:muffin_banana", {
	description = S("Banana Muffin"),
	inventory_image = "farming_muffin_banana.png",
	on_use = minetest.item_eat(4),
})

minetest.register_craft({ 
	type = "shapeless",
	output = "farming:muffin_banana 2",
	recipe = {"ethereal:banana", "group:food_bread", "ethereal:banana"},
})

-- Chocolate

minetest.register_craftitem("farming:muffin_chocolate", {
	description = S("Chocolate Muffin"),
	inventory_image = "farming_muffin_chocolate.png",
	on_use = minetest.item_eat(4),
})

minetest.register_craft({ 
	type = "shapeless",
	output = "farming:muffin_chocolate 2",
	recipe = {"farming:chocolate_dark", "group:food_bread", "farming:chocolate_dark"},
})

-- Raspberries

minetest.register_craftitem("farming:muffin_raspberry", {
	description = S("Raspberry Muffin"),
	inventory_image = "farming_muffin_raspberry.png",
	on_use = minetest.item_eat(4),
})

minetest.register_craft({ 
	type = "shapeless",
	output = "farming:muffin_raspberry 2",
	recipe = {"farming:raspberries", "group:food_bread", "farming:raspberries"},
})
-- Pumpkin Pie

minetest.register_craftitem("farming:pumpkin_pie", {
	description = S("Pumpkin Pie"),
	inventory_image = "farming_pumpkin_pie.png",
	on_use = minetest.item_eat(14),
})

minetest.register_craft({
	output = "farming:pumpkin_pie",
	recipe = { 
		{"farming:baking_tray", "farming:sugar", "farming:whipping_cream"},
		{"farming:pumpkin_slice", "farming:pumpkin_slice", "farming:pumpkin_slice"},
		{"group:food_flour", "group:food_flour", "group:food_flour"},
	},
	replacements = {
		{"farming:baking_tray", "farming:baking_tray"},	
		{"farming:whipping_cream", "vessels:drinking_glass"},
	},
})
-- Brown Sugar

minetest.register_craftitem("farming:brown_sugar", {
	description = S("Brown Sugar"),
	inventory_image = "farming_brown_sugar.png",
})

minetest.register_craft({
	type = "cooking",
	cooktime = 8,
	output = "farming:brown_sugar",
	recipe = "farming:sugar",
})

-- Caramel

minetest.register_craftitem("farming:caramel", {
	description = S("Caramel"),
	inventory_image = "farming_caramel.png",
	on_use = minetest.item_eat(3),
})

minetest.register_craft({
	type = "shapeless",
	output = "farming:caramel",
	recipe = {"farming:brown_sugar", "farming:brown_sugar", "farming:brown_sugar", "farming:brown_sugar"},
})

-- Caramel Apple

minetest.register_craftitem("farming:caramel_apple", {
	description = S("Caramel Apple"),
	inventory_image = "farming_caramel_apple.png",
	on_use = minetest.item_eat(5, "default:stick"),
})

minetest.register_craft({
	output = "farming:caramel_apple",
	recipe = {
		{"", "default:stick", ""}, 
		{"", "default:apple", ""},
		{"", "farming:caramel", ""},	
	},	
})
	
-- Raspberry Pie

minetest.register_craftitem("farming:raspberry_pie", {
	description = S("Raspberry Pie"),
	inventory_image = "farming_raspberry_pie.png",
	on_use = minetest.item_eat(12),
})

minetest.register_craft({
	type = "shapeless",
	output = "farming:raspberry_pie",
	recipe = {"group:food_flour", "farming:sugar", "farming:raspberries", "farming:baking_tray"},

	replacements = {
		{"farming:baking_tray", "farming:baking_tray"},
	},

})

-- Strawberry Pie

minetest.register_craftitem("farming:strawberry_pie", {
	description = S("Strawberry Pie"),
	inventory_image = "farming_strawberry_pie.png",
	on_use = minetest.item_eat(12),
})

minetest.register_craft({
	type = "shapeless",
	output = "farming:strawberry_pie",
	recipe = {"group:food_flour", "farming:sugar", "ethereal:strawberry", "farming:baking_tray"},
	
	replacements = {
		{"farming:baking_tray", "farming:baking_tray"},
	},

})

-- Apple Pie

minetest.register_craftitem("farming:apple_pie", {
	description = S("Apple Pie"),
	inventory_image = "farming_apple_pie.png",
	on_use = minetest.item_eat(12),
})

minetest.register_craft({
	type = "shapeless",
	output = "farming:apple_pie",
	recipe = {"group:food_flour", "farming:sugar", "default:apple", "farming:baking_tray"},

	replacements = {
		{"farming:baking_tray", "farming:baking_tray"},
	},

})
-- Mangrove Apple Pie

minetest.register_craftitem("farming:mangrove_apple_pie", {
	description = S("Mangrove Apple Pie"),
	inventory_image = "farming_mangrove_apple_pie.png",
	on_use = minetest.item_eat(12),
})

minetest.register_craft({
	type = "shapeless",
	output = "farming:mangrove_apple_pie",
	recipe = {"group:food_flour", "farming:sugar", "australia:mangrove_apple", "farming:baking_tray"},

	replacements = {
		{"farming:baking_tray", "farming:baking_tray"},
	},

})
